# CS 1440 Assignment 4: Refactoring & Design Patterns


## 4.0: Refactoring
* [Instructions](instructions/Instructions-4.0.md)
* [Hints](instructions/Hints-4.0.md)
* [Rubric](instructions/Rubric-4.0.md)

## Overview

Our firm has been contracted to help a mathematician take his amazing
one-million dollar idea to market.  Our client specializes in the field of
complex dynamics, which, frankly, is well above my pay grade, but so is
programming to him.  He has a passion for mathematics education and wants to
take his programs to the K-12 system to teach middle and high-school students
all about the beauty of complex numbers and repeated, tedious calculations.  I
didn't have the heart to tell him that there are already dozens of websites
doing what he wants to do for free; if his inability to use Google keeps us in
steady work, who am I to set him straight?

He has created two prototype programs intended for high school math students.
He quickly realized that creating user-friendly software is perhaps more
difficult than understanding complex dynamics.  This is where we come in.

Our contract is to adapt his programs into a complete *Programming Systems
Product*.  We must also make it usable by non-programmers, which means that
instead of controlling the program by changing hard-coded data within the
source code it must accept configuration files from the command-line and
adjust its runtime behavior accordingly.

Now, I realize that asking a user to create configuration files and run a
program from the command-line is no longer considered user-friendly in the
21st century.  We have two teams working on this project: one team will be
creating a GUI which is what the students will ultimately interact with.  This
GUI will drive the core program that you will create.  Your responsibility is
to make sure that your piece of the Program System adheres to the
configuration file format that the GUI team has defined, as well as the
command-line interface they are expecting.


## Running the starter code

These programs given have a simple command line syntax:

    python src/mbrot_fractal.py [FRACTAL_NAME]
    python src/julia_fractal.py [FRACTAL_NAME]

`FRACTAL_NAME` is the name of a fractal image this program is capable of
producing.  When you run either program without an argument it will list names
of images it can draw and exits.

If you use PyCharm you should create **Run configurations** to launch the
program with various arguments.

