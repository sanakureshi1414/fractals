# Fractal Visualizer User Manual

1. Launch this program on terminal or on the terminal on PyCharm 
2. To run this program you must enter one of these commands in the command line interface and press enter. If you are   
   running this program on pycharm insert "src/" in front of the filename.
   
   Running the program on Terminal
   
        python3 main.py [FRACTALNAME]
      
   Running the program on PyCharm 
   
        pyhon3 src/main.py [FRACTALNAME]
       
   *Enter a valid command in order to run this program. If you enter a invalid command, you will displayed with this error. Please re-type your command in order 
    to run the program.
    
        can't open file 'julia_fractl.py': [Errno 2] No such file or directory
        
3. A menu option wil be displayed for you after you enter the command.

    (i) If you ran julia_fractal.py, you will be displayed with the following menu option.

        Please provide the name of a fractal as an argument
        fulljulia
        hourglass
        lakes
        mandelbrot
        spiral0
        spiral1
        seahorse
        elephants
        leaf
        
5. Choose which fractal shape you want to see output and type the fractal name as an argument and press enter
   to display the output.
   
       python3 main.py [FRACTALNAME]
       python3 src/main.py [FRACTALNAME]

       
   *Enter a valid command in order to run this program. If you enter a invalid command, you will displayed with this error. Please re-type your command in order 
    to run the program.
    
        ERROR: hourglas is not a valid fractal
    
6. After you enter the command a Tkinter window with the fractal of your choice will be displayed for you.
       
       cd
 