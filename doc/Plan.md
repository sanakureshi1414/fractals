*Replace the bold text with your own content*

*Adapted from https://htdp.org/2020-5-6/Book/part_preface.html*

# 0.  From Problem Analysis to Data Definitions

This assignment requires us to refactor the working/functional code that is given to us. This means that I need to 
re-write certain parts of code in a more clear and concise way. By meeting these objectives, we can make it easier for
ourselves to better approach the functional code. Following the objectives will be my first approach towards this 
assignment. My second approach to this assignment will be experimenting with the starter code to understand the overall 
functionality of the code. My third and final approach will be to try to make the code as readable as I can without 
changing too much.

The objective of this assignment is to better understand code that is not written by myself.This assignment requires to 
take messy code and turn it into something that is cleaner and concise yet still have it functioning the same as before.
In general, I need to understand the code written by another person, practice reading messy code, organize code, and 
spot "code smells". Following the objectives, I need to understand the starter code and split it into more understandable
sections, organizing my plan, and write code that is functional and translates the "messy" code that we started with.
I will be creating 6 different modules which will do a specific thingThe 6 modules will be main.py, Gradient.py, 
ImagePainter.py, Mandelbrot, Julia, Config.py. 

The requirement specifications are that we identify "code smells" and turn them into something more meaningful.Examples 
of this would be identifying unnecessary comments and bits of code that can be deleted from the starter code, code that 
is repeated and functions that use global variables and/or do unrelated things. 

Code Smells Identified in julia_fractal.py and mbrot_fractal.py
1. Use of global variables: Some functions in the starter code use GLOBAL variables that are discouraged. These should 
   be changed to variables that we pass into the functions. When put into classes these variables should be initialized
   and then used in the function as needed.

    Examples: 
    
    - global win
    - global grad 
    - global WHITE

2. Unnecessary comments: Some comments in the starter code are unnecessary and make no sense and/or are describing the 
   wrong details of what the code is doing. Comments should be changed as necessary to match the functionality of the 
   code.

    Examples: 
  
    - comment: Here 76 refers to the number of colors in the gradient)
    - """Paint a Fractal image into the TKinter PhotoImage canvas.
      This code creates an image which is 640x640 pixels in size."""
    - For convenience I have placed these into a dictionary so you may easily
      switch between them by entering the name of the image you want to generate
      
3. Extra lines of code that are repetitive or don't have any use 

   Examples: 
  
   - canvas.pack() - called too many times, its called at least 4 times at once 
   - return gradient[MAX_ITERATIONS]), repeated twice only one is fine
   
4. Functions that do unrelated things: Some functions in the starter code have code that doesnt have any sort of 
   functionality and/or not used in the code at all. These functions should be deleted and corrected to match what the 
   comment states or vice versa. 

   Examples: 
  
   - The comments and the code do not match in this exmaple: 
       def getFractalConfigurationDataFromFractalRepositoryDictionary(dictionary, name):  
       Make sure that the fractal configuration data repository dictionary
       contains a key by the name of 'name'
       
           When the key 'name' is present in the fractal configuration data repository
           dictionary, return its value.
          
           Return False otherwise
           """
           for key in dictionary:
             if key in dictionary:
                if key == name:
                value = dictionary[key]
                return key
                
   - """Paint a Fractal image into the TKinter PhotoImage canvas.
      This code creates an image which is 640x640 pixels in size."""
   - For convenience I have placed these into a dictionary so you may easily
      switch between them by entering the name of the image you want to generate
   - julia_fractal.py -> fraction = int(512 / 64) is never used in the code 

5. Poorly named identifiers: Some functions have poorly named identifiers. It is hard to understand what a poorly 
   named variable does and where its used which is why these should be changed to more effective names that make it 
   obvious what the identifier does and where its being used.

   Examples: 
  
   - for r ...... - r should be renamed to row 
   - for c ...... - c should be renamed to col
   - pixelsize ..... - should be renamed pixelSize
   

# 1.  System Analysis

**User Interface:**
From analyzing the data throughout both of the programs, julia_fractal.py and mbrot_fractal.py I see that both 
programs have a main function where we ask for user input in order to output the Tk window with the selected shape. 
The user is asked to manually select and type in their choice of shape that they would like to see outputted to the 
screen. This will be done in the command interface. Depending on the choice, a Tk window will be displayed with the
selected fractal shape.

**Output:**
The output is displayed through a Tkinter Window. In order to use Tkinter, the program imports its functions and uses 
it to produce and display the desired output. The output is a graphic display, its a colored shaped called a fractal 
that is a result of a calculation of complex numbers.

**Formulas:**
No formulas that I need to create, just use the one given in the starter code. The calculations will the needed to find 
X and Y coordinated at which we can apply a color at given point to create the output on the Tkinter window.

**Functions**
Both programs have similar functions. Each have their own unique functionality. The common functions between the two 
programs are the main and the one where the get the color of the pixel and create the gradient for the output.

# 2.  Functional Examples

Working examples of code from julia_fractal.py: The following is an example of how the code will look like when 
most of the code smells present in the code are deleted and chnaged to make more the code cleaner and understandable.

    # def getColorFromGradient(z, gradient):
    """Return the index of the color of the current pixel within the Julia set
    in the gradient array"""
    
        # c is the Julia Constant; varying this value can yield interesting images
        c = complex(-1.0, 0.0)
 
        for i in range(78):
            z = z * z + c  # Iteratively compute z1, z2, z3 ...
            # check to see if the absoclute value of the computation of z is greater than 2, 
            if greater than 2:
                calculate z 
                return gradient the gradient color at index i
                
            if abs(z) > 2:
                z += z + c
                return gradient[i]  
                # return the gradient at index i for whatever the computation is for complex numbers 
            
    # return the gradient at index 77 
    return gradient[77]         

    def getFractalConfigurationDataFromFractalRepositoryDictionary(dictionary, name):
    """Make sure that the fractal configuration data repository dictionary
    contains a key by the name of 'name'
    
        When the key 'name' is present in the fractal configuration data repository
        dictionary, return its value. # delete comment and also contradicting comment 
    
        Return False otherwise # implementation does not include return statement
        """
        for key in dictionary:
            if key in dictionary:
                if key == name:
                    value = dictionary[key]
                    return key
                else:
                    return False    
                    
    def makePicture(f, i, e, win, gradient, photo):
    """Paint a Fractal image into the TKinter PhotoImage canvas.
    Assumes the image is 512x512 pixels."""

    # Correlate the boundaries of the PhotoImage object to the complex
    # coordinates of the imaginary plane
    min = ((f['centerX'] - (f['axisLength'] / 2.0)),
           (f['centerY'] - (f['axisLength'] / 2.0)))

    max = ((f['centerX'] + (f['axisLength'] / 2.0)),
           (f['centerY'] + (f['axisLength'] / 2.0)))
           
    # Display the image on the screen
    canvas = Canvas(win, width=512, height=512, bg=WHITE)
    canvas.pack()
    canvas.create_image((256, 256), image=photo, state="normal")

    area_in_pixels = 512 * 512

    # At this scale, how much length and height of the
    # imaginary plane does one pixel cover?
    size = abs(max[0] - min[0]) / 512.0

    fraction = int(512 / 64)
    for r in range(512, 0, -1):
        for c in range(512):
            x = min[0] + c * size
            y = min[1] + r * size
            c2 = getColorFromGradient(complex(x, y))
            photo.put(c2, (c, 512 - r))
        win.update()  # display a row of pixels
              
                                  
Good and Working examples of code from mbrot_fractal.py: The following examples are corrections made on the starter 
code with most of the code smells deleted and replaced with code that makes sense yet still maintaining the 
functionality of the program and the function specifically.

    MAX_ITERATIONS = len(gradient)
    z = 0
    
    def colorOfThePixel(c, gradient):
    """Return the color of the current pixel within the Mandelbrot set"""
        z = complex(0, 0)  # z0
        
        for i in range(MAX_ITERATIONS):
            z = z * z + c  # Get z1, z2, ...
            if abs(z) > 2:
                z = 2.0
                return gradient[i]  # The sequence is unbounded
    
    return gradient[MAX_ITERATIONS - 1]   
    
    def paint(fractals, imageName, gradient, img):
    """Paint a Fractal image into the TKinter PhotoImage canvas.
    This code creates an image which is 640x640 pixels in size."""
    
    fractal = fractals[imageName]

    # Figure out how the boundaries of the PhotoImage relate to coordinates on
    # the imaginary plane.
    minX = fractal['centerX'] - (fractal['axisLen'] / 2.0)
    maxX = fractal['centerX'] + (fractal['axisLen'] / 2.0)
    minY = fractal['centerY'] - (fractal['axisLen'] / 2.0)
    maxY = fractal['centerY'] + (fractal['axisLen'] / 2.0)

    # Display the image on the screen
    canvas = Canvas(window, width=512, height=512, bg='#ffffff')
    canvas.pack()
    canvas.create_image((256, 256), image=img, state="normal")

    # At this scale, how much length and height on the imaginary plane does one
    # pixel take?
    pixelsize = abs(maxx - minx) / 512

    portion = int(512 / 64)
    total_pixels = 1048576
    for row in range(512, 0, -1):
        for col in range(512):
            x = minx + col * pixelsize
            y = miny + row * pixelsize
            color = colorOfThePixel(complex(x, y), gradient)
            img.put(color, (col, 512 - row))
        window.update()  # display a row of pixels

# 3.  Function Template

For the function templates, I will be writing the starter code in pseudocode which in step 4 will be written in 
actual code that runs without errors.

julia_fractal.py 

    def getColorFromGradient(z, gradient):
    """Return the index of the color of the current pixel within the Julia set
    in the gradient array"""
    
        set variaable c to a winow size that is complex(-1.0, 0.0)
 
         for i in the range of 78 as the gradient color 
             check to see if the absoclute value of the computation of z is greater than 2, 
             if greater than 2:
                 calculate z 
                    return gradient the gradient color at index i
          
         return the gradient at index 77
         
     
    def getFractalConfigurationDataFromFractalRepositoryDictionary(dictionary, name):
    """Make sure that the fractal configuration data repository dictionary
    contains a key by the name of 'name'
    
        When the key 'name' is present in the fractal configuration data repository
        dictionary, return its value.  
    
        Return False otherwise 
        """
        
        for key in the dictionary:
            check if key is in the dictionaty:
               if key is equal to name:
                    return the dictionary at key and set this to a variable value 
                    return key 
                else: 
                    return False if the key is not detected in the dictionary 
    
    def makePicture(f, i, e, win, gradient, photo):
    """Paint a Fractal image into the TKinter PhotoImage canvas.
    Assumes the image is 512x512 pixels."""

    min = ((f['centerX'] - (f['axisLength'] / 2.0)),
           (f['centerY'] - (f['axisLength'] / 2.0)))

    max = ((f['centerX'] + (f['axisLength'] / 2.0)),
           (f['centerY'] + (f['axisLength'] / 2.0)))
           
    create a canvas with window, width and height at 512 and background color of #ffffff
    use the .pack() to create the canvas 
    create the canvas with an image with dimensions 256 * 256 , and set image to img and the state the normal

    set area_in_pixels to 512 * 512

    set size euqual to the abs(max[0] - min[0]) / 512.0

    for c in range of 512, 0, and counting backwards:
        x  is euqal to minimum at index 0 + c * size 
        y is equal to minimum at index 1 +r * size 
    set fraction to an int(512 / 64)
    for r in range(512, 0, -1):
        for c in range(512):
            set x to min[0] + c * size
            set y to min[1] + r * size
            set c2 to the built in function getColorFromGradient(complex(x, y))
            set photo.put(c2, (c, 512 - r))
        update the window
        
        
    create a main 
    get user input 
    check if user input is valid 
    if user input length is less then sys.argv then not a valid input 
        print "please provide the name of a fractal as an argument"
        for i in f:
            print()
        exit the program 
    else if sys.argv index 1 is not in f 
        print "please choose one of the following"
        for i in f:
            print()
        diplay the shape 
        exit the program
        

mbrot_fractal.py 

    MAX_ITERATIONS = len(gradient)
    z = 0
    
    def colorOfThePixel(c, gradient):
    """Return the color of the current pixel within the Mandelbrot set"""
        z = complex(0, 0)  # z0
        
        for i in range (MAX_ITERATIONS):
            calclate z 
            if absolute value of z is greater than 2 then
                set z = 2.0 
                return gradient at index i 
               
    return gradient at MAX_ITERATIONS - 1
    
    minX = fractal['centerX'] - (fractal['axisLen'] / 2.0)
    maxX = fractal['centerX'] + (fractal['axisLen'] / 2.0)
    minY = fractal['centerY'] - (fractal['axisLen'] / 2.0)
    maxY = fractal['centerY'] + (fractal['axisLen'] / 2.0)
    
    create a canvas with window, width and height at 512 and background color of #ffffff
    use the .pack() to create the canvas 
    create the canvas with an image with dimensions 256 * 256 , and set image to img and the state the normal

    pixelsize is abs(maxx - minx) / 512

    set portion to int(512 / 64)
    set total_pixels to 1048576
    for row in range(512, 0, -1):
        for col in range(512):
            set x to minx + col * pixelsize
            set y to miny + row * pixelsize
            set the color to the colorOfThePixel at (complex(x, y), gradient)
            set the image to img.put(color, (col, 512 - row))
        display the window and update it
        
  
    create a main 
    get user input 
    check if user input is valid 
    if user input length is less then sys.argv then not a valid input 
        print "please provide the name of a fractal as an argument"
        for i in f:
            print()
        exit the program 
    else if sys.argv index 1 is not in f 
        print "please choose one of the following"
        for i in f:
            print()
        diplay the shape 
        exit the program
        
# 4.  Implementation

main.py 

    def getInput(listOfFractals):
        if len(sys.argv) < 2:
            print("Please provide the name of a fractal as an argument")
            for i in listOfFractals:
                print(f"\t{i}")
            sys.exit(1)
    
        elif sys.argv[1] not in Config.listOfFractals:
            print(f"ERROR: {sys.argv[1]} is not a valid fractal")
            print("Please choose one of the following:")
            for i in listOfFractals:
                print(f"\t{i}")
            sys.exit(1)
    
        else:
            return Config.listOfFractals[sys.argv[1]]

    def main():
        fractal = getInput(Config.listOfFractals)
    
        # create window to display the image
        window = Tk()
        img = PhotoImage(width=512, height=512)
        ImagePainter.paintPicture(Config.listOfFractals, fractal)
    
        # Save the image as a PNG
        img.write(f"{fractal}.png")
        print(f"Wrote image {fractal}.png")
    
        mainloop()
    
        main()

Config.py 

    class Config:
        def __init__(self, dictionary, name):
            self.dictionary = dictionary
            self.name = name
    
        def getFractalConfigurationData(self):
            """Make sure that the fractal configuration data repository dictionary
            contains a key by the name of 'name'
            """
            for key in self.dictionary:
                if key in self.dictionary:
                    if key == self.name:
                        value = self.dictionary[key]
                        return value
                else:
                    return False
    
        listOfFractals = {
            'fulljulia': {
                'type': 'Julia',
                'centerX': 0.0,
                'centerY': 0.0,
                'axisLength': 4.0,
            },
    
            'hourglass': {
                'type': 'Julia',
                'centerX': 0.618,
                'centerY': 0.00,
                'axisLength': 0.017148277367054,
            },
    
            'lakes': {
                'type': 'Julia',
                'centerX': -0.339230468501458,
                'centerY': 0.417970758224314,
                'axisLength': 0.164938488846612,
            },
    
            'mandelbrot': {
                'type': 'Mandelbrot',
                'centerX': -0.6,
                'centerY': 0.0,
                'axisLength': 2.5,
            },
    
            'spiral0': {
                'type': 'Mandelbrot',
                'centerX': -0.761335372924805,
                'centerY': 0.0835704803466797,
                'axisLength': 0.004978179931102462,
            },
    
            'spiral1': {
                'type': 'Mandelbrot',
                'centerX': -0.747,
                'centerY': 0.1075,
                'axisLength': 0.002,
            },
    
            'seahorse': {
                'type': 'Mandelbrot',
                'centerX': -0.745,
                'centerY': 0.105,
                'axisLength': 0.01,
            },
    
            'elephants': {
                'type': 'Mandelbrot',
                'centerX': 0.30820836067024604,
                'centerY': 0.030620936230004017,
                'axisLength': 0.03,
            },
    
            'leaf': {
                'type': 'Mandelbrot',
                'centerX': -1.543577002,
                'centerY': -0.000058690069,
                'axisLength': 0.000051248888,
            },
        }

ImagePainter.py 

    class ImagePainter:
        def __init__(self, pixels, nameOfFractal):
            self.pixels = pixels
            self.nameOfFractal = nameOfFractal
            self.listOfFractals = Config.listOfFractals
    
        def setUpGUI(self):
            window = Tk()
            photo = PhotoImage(width=self.pixels, height=self.pixels)
            return window, photo
    
        def paintPicture(self, listOfFractals):
            window, photo = ImagePainter.setUpGUI(self.pixels)
    
            lower = ((listOfFractals['centerX'] - (listOfFractals['axisLength'] / 2.0)),
                     (Config.listOfFractals['centerY'] - (listOfFractals['axisLength'] / 2.0)))
    
            upper = ((listOfFractals['centerX'] + (listOfFractals['axisLength'] / 2.0)),
                     (listOfFractals['centerY'] + (listOfFractals['axisLength'] / 2.0)))
    
            # Create the Canvas and Display the image
            canvas = Canvas(window, width=512, height=512, bg='#ffffff')
            canvas.create_image((256, 256), image=photo, state="normal")
            canvas.pack()
    
            # Loop through the row and column and determine if nameOfFractal at 'type' is julia or mandelbrot
            # and display the picture
            for row in range(self.pixels, 0, -1):
                for col in range(self.pixels):
                    pixelSize = abs(upper[0] - lower[0]) / 512
                    x = lower[0] + col * pixelSize
                    y = lower[1] + row * pixelSize
                    if Config.listOfFractals[self.nameOfFractal]['type'] == 'Julia':
                        colorOfPixel = Gradient[Julia(complex(x, y))]
                        photo.put(colorOfPixel, (self.pixels - row))
                    elif Config.listOfFractals[self.nameOfFractal]['type'] == 'Mandelbrot':
                        colorOfPixel = Gradient[Mandelbrot(complex(x, y))]
                        photo.put(colorOfPixel, (self.pixels - row))
            
            window.update()

Julia.py 

    class Julia:
        def __init__(self, z):
            self.z = z
            self.gradient = Gradient().gradient()
    
        def getColorOfPixel(self):
            """Return the index of the color of the current pixel within the Julia set
               in the gradient array"""
    
            c = complex(-1.0, 0.0)
    
            MAX_ITERATIONS = 96
            for i in range(MAX_ITERATIONS):
                self.z = self.z * self.z + c
                if abs(self.z) > 2:
                    return self.gradient[i]
    
            return self.gradient[77]

Mandelbrot.py 

    class Mandelbrot:
        def __init__(self, c):
            self.c = c
    
        def mandelbrot(self):
            z = complex(0, 0)
    
            MAX_ITERATIONS = 96
            for i in range(MAX_ITERATIONS):
                z = z * z + self.c
                if abs(z) > 2:
                    return i   # the sequence is unbounded
    
            return [MAX_ITERATIONS - 1]   # Indicate the bounded sequence

Gradient.py 

    class Gradient:
        def __init__(self):
            self.gradient = [
                '#ffe4b5', '#ffe5b2', '#ffe7ae', '#ffe9ab', '#ffeaa8', '#ffeda4',
                '#ffefa1', '#fff29e', '#fff49a', '#fff797', '#fffb94', '#fffe90',
                '#fcff8d', '#f8ff8a', '#f4ff86', '#f0ff83', '#ebff80', '#e7ff7c',
                '#e2ff79', '#ddff76', '#d7ff72', '#d2ff6f', '#ccff6c', '#c6ff68',
                '#bfff65', '#b9ff62', '#b2ff5e', '#abff5b', '#a4ff58', '#9dff54',
                '#95ff51', '#8dff4e', '#85ff4a', '#7dff47', '#75ff44', '#6cff40',
                '#63ff3d', '#5aff3a', '#51ff36', '#47ff33', '#3eff30', '#34ff2c',
                '#2aff29', '#26ff2c', '#22ff30', '#1fff34', '#1cff38', '#18ff3d',
                '#15ff42', '#11ff47', '#0eff4c', '#0bff51', '#07ff57', '#04ff5d',
                '#01ff63', '#00fc69', '#00f970', '#00f677', '#00f27d', '#00ef83',
                '#00ec89', '#00e88e', '#00e594', '#00e299', '#00de9e', '#00dba3',
                '#00d8a7', '#00d4ab', '#00d1af', '#00ceb3', '#00cab7', '#00c7ba',
                '#00c4be', '#00c0c0', '#00b7bd', '#00adba', '#00a4b6', '#009cb3',
                '#0093b0', '#008bac', '#0082a9', '#007ba6', '#0073a2', '#006b9f',
                '#00649c', '#005d98', '#005695', '#004f92', '#00498e', '#00438b',
                '#003d88', '#003784', '#003181', '#002c7e', '#00277a', '#002277',
            ]

        def getGradient(self):
            return self.gradient


# 5.  Testing

Step by Step process of the program 
1. Launch the program in terminal or on pycharms terminal 
2. Run this command to run the program:

    (i) julia_fractal: python3 src/julia_fractal [FRACTALNAME]
    
    (ii) mbrot_fractal: python3 src/mbrot_fractal [FRACTALNAME]
    
3. After you run the command, you will see a message with a few options to choose from.
4. Choose your option and type it in the command line in place of [FILENAME] and run the program.
5. Your fractal shape will be diaplyed on a 512 * 512 sized Tkinter window.


To test if my code is correct, I will keep running the unit tests that come with this program and check to see if they 
all pass. The unit tests will be my assurance that my code is functioning as it should be. In the case that the 
unit tests don't pass I will be going back to the specific part of the program where a software error or fault occurred
and fix any bugs that were present or gone unnoticed. 

An example of testing would be to run the "julia_fractal.py" without any code smells and check to see if it works as it
should. I will also check to see if my imports in main.py and other python files are correct and do as they should. As 
for creating new unit tests, I will not be creating any. I will be sticking to the unit tests that are given in this 
assignment and check my progress through them. I will make sure all 4 of my tests pass.
