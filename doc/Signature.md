| Date      | Events
|-----------|--------------------
| November 5 | Read through the requirements specification and wrote Section 0 and Section 1 of SDP
| November 6 | Revised requirements specification and wrote Section 2 and Section 3 of SDP 
| November 7 | Started implementation on julia_fractal.py and wrote Section 4 and Section 5 of SDP and test cases 
| November 8 | Revised SDP and started Manual and UML Diagram
| November 9 | Started implementation on 6 modules and ran test cases against for bugs and fix as I move forward
| November 10 | Continued implementation on 6 modules and completed Manual 
| November 11 | Continued implementation on 6 modules
| November 12 | Test cases for assignment and fix any bugs
| November 13 | Finalize manual and SDP as well as 6 modules, and submit assignment through a tag "Assn4.o"
| November 15 | Late submission, you used grading gift today. 
