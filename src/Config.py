class Config:
    def __init__(self, dictionary, name):
        self.dictionary = dictionary
        self.name = name

    def getFractalConfigurationData(self):
        """Make sure that the fractal configuration data repository dictionary
        contains a key by the name of 'name'
        """
        for key in self.dictionary:
            if key in self.dictionary:
                if key == self.name:
                    value = self.dictionary[key]
                    return value
            else:
                    return False

    listOfFractals = {
        'fulljulia': {
            'type': 'Julia',
            'centerX': 0.0,
            'centerY': 0.0,
            'axisLength': 4.0,
        },

        'hourglass': {
            'type': 'Julia',
            'centerX': 0.618,
            'centerY': 0.00,
            'axisLength': 0.017148277367054,
        },

        'lakes': {
            'type': 'Julia',
            'centerX': -0.339230468501458,
            'centerY': 0.417970758224314,
            'axisLength': 0.164938488846612,
        },

        'mandelbrot': {
            'type': 'Mandelbrot',
            'centerX': -0.6,
            'centerY': 0.0,
            'axisLength': 2.5,
        },

        'spiral0': {
            'type': 'Mandelbrot',
            'centerX': -0.761335372924805,
            'centerY': 0.0835704803466797,
            'axisLength': 0.004978179931102462,
        },

        'spiral1': {
            'type': 'Mandelbrot',
            'centerX': -0.747,
            'centerY': 0.1075,
            'axisLength': 0.002,
        },

        'seahorse': {
            'type': 'Mandelbrot',
            'centerX': -0.745,
            'centerY': 0.105,
            'axisLength': 0.01,
        },

        'elephants': {
            'type': 'Mandelbrot',
            'centerX': 0.30820836067024604,
            'centerY': 0.030620936230004017,
            'axisLength': 0.03,
        },

        'leaf': {
            'type': 'Mandelbrot',
            'centerX': -1.543577002,
            'centerY': -0.000058690069,
            'axisLength': 0.000051248888,
        },
    }



