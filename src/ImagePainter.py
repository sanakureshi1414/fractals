# Creates a Tk window and a PhotoImage object; the PhotoImage stores the pixels and is capable of creating a PNG image file

from tkinter import Tk, PhotoImage, Canvas
from Config import Config
from Mandelbrot import Mandelbrot
from Julia import Julia

class ImagePainter:
    def __init__(self, pixels, nameOfFractal):
        self.pixels = pixels
        self.nameOfFractal = nameOfFractal
        self.listOfFractals = Config.listOfFractals

    def setUpGUI(self, pixels):
        window = Tk()
        photo = PhotoImage(width=pixels, height=pixels)
        return window, photo

    def paintPicture(self, fractal):
        window, photo = self.setUpGUI(self.pixels)

        lower = ((fractal['centerX'] - (fractal['axisLength'] / 2.0)),
                 (fractal['centerY'] - (fractal['axisLength'] / 2.0)))

        upper = ((fractal['centerX'] + (fractal['axisLength'] / 2.0)),
                 (fractal['centerY'] + (fractal['axisLength'] / 2.0)))

        # Create the Canvas and Display the image
        canvas = Canvas(window, width=512, height=512, bg='#ffffff')
        canvas.pack()
        canvas.create_image((256, 256), image=photo, state="normal")

        # Loop through the row and column and determine if nameOfFractal at 'type' is julia or mandelbrot
        # and display the picture
        for row in range(self.pixels, 0, -1):
            for col in range(self.pixels):
                pixelSize = abs(upper[0] - lower[0]) / 512
                x = lower[0] + col * pixelSize
                y = lower[1] + row * pixelSize
                if Config.listOfFractals[self.nameOfFractal]['type'] == 'Julia':
                    colorOfPixel = Julia(complex(x, y)).getColorOfPixel()
                    photo.put(colorOfPixel, (col, self.pixels - row))
                elif Config.listOfFractals[self.nameOfFractal]['type'] == 'Mandelbrot':
                    colorOfPixel = Mandelbrot(complex(x, y)).getColorOfPixel()
                    photo.put(colorOfPixel, (col, self.pixels - row))
        
            window.update()


