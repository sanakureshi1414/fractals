from Gradient import Gradient

class Julia:
    def __init__(self, z):
        self.z = z
        self.gradient = Gradient()

    def getColorOfPixel(self):
        """Return the index of the color of the current pixel within the Julia set
           in the gradient array"""

        coordinates = complex(-1.0, 0.0)

        MAX_ITERATIONS = 96
        for i in range(MAX_ITERATIONS):
            self.z = self.z * self.z + coordinates
            if abs(self.z) > 2:
                return self.gradient.getGradient(i)

        return self.gradient.getGradient(77)

