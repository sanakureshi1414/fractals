from Gradient import Gradient

class Mandelbrot:
    def __init__(self, c):
        self.c = c
        self.gradient = Gradient()

    def getColorOfPixel(self):
        """Return the index of the color of the current pixel within the Julia set
           in the gradient array"""

        z = complex(0, 0)

        MAX_ITERATIONS = 96
        for i in range(MAX_ITERATIONS):
            z = z * z + self.c
            if abs(z) > 2:
                return self.gradient.getGradient(i)

        return self.gradient.getGradient(MAX_ITERATIONS - 1)


