import sys
from tkinter import Tk, Canvas, PhotoImage, mainloop
from Config import Config
from ImagePainter import ImagePainter

# The driver program; imports other modules, accepts command-line arguments and calls upon other modules
# to display a fractal on-screen and write a PNG image. This file is the main entry point of the program.

def getInput(listOfFractals):
    if len(sys.argv) < 2:
        print("Please provide the name of a fractal as an argument")
        for i in listOfFractals:
            print(f"\t{i}")
        sys.exit(1)

    elif sys.argv[1] not in Config.listOfFractals:
        print(f"ERROR: {sys.argv[1]} is not a valid fractal")
        print("Please choose one of the following:")
        for i in listOfFractals:
            print(f"\t{i}")
        sys.exit(1)

    else:
        return listOfFractals[sys.argv[1]]


def main():
    fractal = getInput(Config.listOfFractals)
    pixels = 512

    # create window to display the image
    # window = Tk()
    imagePainter = ImagePainter(pixels, sys.argv[1])
    imagePainter.paintPicture(fractal)

    # Save the image as a PNG
    img = PhotoImage(width=512, height=512)
    img.write(f"{fractal}.png")
    print(f"Wrote image {fractal}.png")

    mainloop()


main()

