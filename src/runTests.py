import unittest

from Testing import TestMandelbrot
from Testing import TestJulia


suite = unittest.TestSuite()
tests = (TestMandelbrot.TestMandelbrot, TestJulia.TestJulia)
for test in tests:
    suite.addTest(unittest.makeSuite(test))

runner = unittest.TextTestRunner(verbosity=2)
runner.run(suite)
